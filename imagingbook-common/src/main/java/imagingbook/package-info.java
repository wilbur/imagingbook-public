/*******************************************************************************
 * This software is provided as a supplement to the authors' textbooks on digital
 * image processing published by Springer-Verlag in various languages and editions.
 * Permission to use and distribute this software is granted under the BSD 2-Clause 
 * "Simplified" License (see http://opensource.org/licenses/BSD-2-Clause). 
 * Copyright (c) 2006-2015 Wilhelm Burger, Mark J. Burge. 
 * All rights reserved. Visit http://www.imagingbook.com for additional details.
 *  
 *******************************************************************************/
/**
 * This package contains the common library used by the ImageJ plugins
 * provided for Digital Image Processing books authored by Wilhelm Burger and Mark J Burge,
 * published by Springer.
 * See also
 * <a href="http://www.imagingbook.com">http://www.imagingbook.com</a> and
 * <a href="https://sourceforge.net/projects/imagingbook/">https://sourceforge.net/projects/imagingbook/</a>.
 * <p>
 * Permission to use and distribute this software is granted
 * under the BSD 2-Clause "Simplified" License (see
 * <a href="http://opensource.org/licenses/BSD-2-Clause">http://opensource.org/licenses/BSD-2-Clause</a>).
 * Copyright (c) 2006-2015 Wilhelm Burger, Mark J. Burge. 
 * All rights reserved.
 * <p>
 * Requires Java 1.5 or higher.
 * @author Wilhelm Burger and Mark J. Burge
 */
package imagingbook;